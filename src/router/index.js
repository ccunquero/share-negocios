import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import VueCurrencyInput from 'vue-currency-input'
import util from '../mixin/Util'

import LottieAnimation from "lottie-vuejs/src/LottieAnimation.vue"; // import lottie-vuejs
 
Vue.use(LottieAnimation); // add lottie-animation to your global scope
 

const pluginOptions = {
  /* see config reference */
  globalOptions: { currency: 'USD',}
}

import CryptoJS from 'crypto-js';
Vue.use(VueCurrencyInput ,pluginOptions)
Vue.use(VueRouter)


/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function ( { store, ssrContext } ) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
  

    let isAuthenticated = false
    if(localStorage.getItem("hash")){
      var message = localStorage.getItem("hash")
      var code = CryptoJS.AES.decrypt(message, localStorage.getItem("userKey").substr(0,5));
      var decryptedMessage = code.toString(CryptoJS.enc.Utf8);

      if(decryptedMessage == localStorage.getItem("userKey"))
      isAuthenticated = true
    }
    //Si en el login se obitene el supervisorId, significa que es asesor 
    let isAsesor = localStorage.getItem("supervisorId")
    if (to.name !== 'Login' && !isAuthenticated) 
      next({ name: 'Login' , })
    else if(to.name == "Login" && isAuthenticated) 
      next({name :  "Dashboard"}) 
    else if(to.name == "Init" && isAuthenticated) 
      next({name :  "Dashboard"}) 
    else if(to.name == "CreditPlanner" && isAsesor) 
      next({name: 'CreditPlannerAdviser', params: {  id: to.params.id}})
    else if(to.name == "CreditPlannerAdviser" && !isAsesor) 
      next({name: 'CreditPlanner', params: {  id: to.params.id}})
    else if(to.name == "CollectionsManager" && isAsesor) 
      next({name: 'CollectionsManagerAdviser', params: {  id: to.params.id}})
    else if(to.name == "CollectionsManagerAdviser" && !isAsesor) 
      next({name: 'CollectionsManager', params: {  id: to.params.id}})
    else if(to.name == "PortfolioMonitoringAdviser" && !isAsesor) 
      next({name: 'PortfolioMonitoring', params: {  id: to.params.id}})
      else if(to.name == "PortfolioMonitoring" && isAsesor) 
      next({name: 'PortfolioMonitoringAdviser', params: {  id: to.params.id}})
    else   next()

  })

  return Router
}
