const routes = [
  {
    path: "/",
    name: "Init",
    component: () => import("pages/Login.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("pages/Login.vue")
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: () => import("pages/Index.vue")
  },
  {
    path: "/credit-planner/:id",
    name: "CreditPlanner",
    component: () => import("src/pages/CreditPlanner.vue")
  },
  {
    path: "/credit-planner/adviser/:id",
    name: "CreditPlannerAdviser",
    component: () => import("src/pages/CreditPlannerAdviser.vue")
  },
  {
    path: "/collections-manager/:id",
    name: "CollectionsManager",
    component: () => import("src/pages/CollectionsManager.vue")
  },
  {
    path: "/collections-manager/adviser/:id",
    name: "CollectionsManagerAdviser",
    component: () => import("src/pages/CollectionsManagerAdviser.vue")
  },
  {
    path: "/portfolio-monitoring/:id",
    name: "PortfolioMonitoring",
    component: () => import("src/pages/PortfolioMonitoring.vue")
  },
  {
    path: "/portfolio-monitoring/adviser/:id",
    name: "PortfolioMonitoringAdviser",
    component: () => import("src/pages/PortfolioMonitoringAdviser.vue")
  },
  {
    path: "/activities/:id",
    name: "Activities",
    component: () => import("src/pages/Activities.vue")
  },

  {
    path: "/facilitating-plan/:id",
    name: "FacilitatingPlan",
    component: () => import("src/pages/FacilitatingPlan.vue")
  },
  {
    path: "/faciliting-monitoring/:id",
    name: "FacilitingMonitoring",
    component: () => import("src/pages/FacilitingMonitoring.vue")
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
