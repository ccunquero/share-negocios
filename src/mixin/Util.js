export const userRole = {
  COORDINATOR: "CORRODINATOR",
  MANGER: "MANAGER"
};
import CryptoJS from "crypto-js";
const crypto = require("crypto");
export default {
  data: () => ({
    USER_LEVEL_3: 3,
    USER_LEVEL_2: 2,
    USER_LEVEL_1: 1,
    //urlBase: "http://181.114.12.130:8787/businessapp/api/business/" //PUblica
    urlBase: "http://localhost:8080/businessapp/api/business/" //Local
    //urlBase: "https://shareappgt.com/api/business/" //Produccion
  }),
  methods: {
    async request(urlComplement, body, addUserKey = true, addUserId = true) {
      console.log(this.urlBase + urlComplement);
      console.log(body);
      if (addUserKey) body.userKey = this.$store.state.userKey;
      if (addUserId) body.idUsu = this.$store.state.userId;

      let promise = new Promise((resolve, reject) => {
        this.$q.loading.show();
        this.$http
          .post(this.urlBase + urlComplement, body, {
            headers: {
              "content-type": "application/json"
            }
          })
          .then(response => {
            this.$q.loading.hide();
            console.log(response);
            if (response) {
              if (response.data.estado == 401) {
                this.notifyErrorAuthentication();
                resolve(null);
              } else if (response.data.estado == 500) {
                this.notifyErrorServer();
                resolve(null);
              } else {
                resolve(response.data);
              }
            } else {
              resolve(null);
              this.notifyError();
            }
          })
          .catch(error => {
            console.log("REQUEST ERROR");
            resolve(null);
            this.notifyError();
            this.$q.loading.hide();
            this.closeSessionUser();
          });
      });

      return await promise;
    },
    notifySuccess(message) {
      this.$q.notify({
        type: "positive",
        message: message ? message : "Operación realizada correctamente"
      });
    },

    notifyError(message) {
      this.$q.notify({
        type: "negative",
        message: message ? message : "Se ha producido un error"
      });
    },

    notifyErrorServer() {
      this.$q.notify({
        type: "negative",
        message: "Se ha producido un error en el servidor"
      });
    },

    notifyErrorAuthentication() {
      this.$q.notify({
        type: "negative",
        message: "Error en la autenticación del usuario"
      });
    },

    notifyWarning(message) {
      this.$q.notify({
        type: "warning",
        message: message ? message : "Advertencia"
      });
    },
    notifyInfo(message) {
      this.$q.notify({
        type: "info",
        message: message ? message : "Informacion"
      });
    },
    getFormatMoney(
      amount,
      decimalCount = 2,
      decimal = ".",
      thousands = ",",
      currency = "Q "
    ) {
      try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(
          (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
        ).toString();
        let j = i.length > 3 ? i.length % 3 : 0;

        return (
          currency +
          negativeSign +
          (j ? i.substr(0, j) + thousands : "") +
          i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
          (decimalCount
            ? decimal +
              Math.abs(amount - i)
                .toFixed(decimalCount)
                .slice(2)
            : "")
        );
      } catch (e) {
        console.log(e);
      }
    },
    encrypt(name, message = "", key = "") {
      var message = CryptoJS.AES.encrypt(message, key);
      localStorage.setItem(name, message.toString());
      return message.toString();
    },
    decrypt(name, key = "") {
      var message = localStorage.getItem(name);
      var code = CryptoJS.AES.decrypt(message, key);
      var decryptedMessage = code.toString(CryptoJS.enc.Utf8);
      return decryptedMessage;
    },
    storeCommits() {
      this.$store.commit("setUserKey", localStorage.getItem("userKey") || null);
      this.$store.commit("setUserId", localStorage.getItem("userId") || null);
      this.$store.commit(
        "setSystemDate",
        localStorage.getItem("systemDate") || null
      );
      this.$store.commit(
        "setUsername",
        localStorage.getItem("username") || null
      );
      this.$store.commit(
        "setOfficeId",
        localStorage.getItem("officeId") || null
      );
      this.$store.commit(
        "setOffice",
        localStorage.getItem("officeName") || null
      );
      this.$store.commit("setRolId", localStorage.getItem("rolId") || null);
      this.$store.commit(
        "setSupervisorId",
        localStorage.getItem("supervisorId") || null
      );
      this.$store.commit(
        "setSupervisorName",
        localStorage.getItem("supervisorName") || null
      );
    },
    getFormarHour(hour) {
      hour = hour;
      let hourString = "";
      hourString += hour + ":00";
      if (hour < 12) hourString += " AM";
      else hourString += " PM";

      return hourString;
    },

    decryptAES(key, cypher) {
      var h = crypto.createHash("sha1");
      h.update(key, "utf8");
      var k = h.digest().slice(0, 16);
      var d = crypto.createDecipheriv("aes-128-ecb", k, Buffer.alloc(0));
      return Buffer.concat([d.update(cypher, "base64"), d.final()]).toString();
    },
    getMonthName(month) {
      let months = "Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre".split(
        "_"
      );
      return months[month - 1];
    },
    isEnableOption(options, id) {
      return options.filter(item => item.idOp == id).length == 1 ? true : false;
    },
    getPartDate(part, date) {
      let tempDate = date.split("/");
      if (part == "D") return tempDate[0];
      else if (part == "M") return tempDate[1];
      else if (part == "Y") return tempDate[2];
    },
    formatToParse(value) {
      return (value + "").split(",").join("") || 0;
    },
    formatTwoDigit(value) {
      if (parseInt(value) < 9) return "0" + parseInt(value);
      else return value;
    },
    getHierarchy() {
      return parseInt(localStorage.getItem("hierarchyId")) || 0;
    },
    closeSessionUser() {
      let password;
      let user;
      if (localStorage.getItem("rememberPassword") == "true") {
        user = localStorage.getItem("user");
        password = localStorage.getItem("password");
      }
      localStorage.clear();
      if (password) {
        localStorage.setItem("user", user);
        localStorage.setItem("password", password);
        localStorage.setItem("rememberPassword", "true");
      }
      localStorage.setItem("notValidateSessionActive", 1);
      this.storeCommits();
      this.$router.push({ name: "Login" });
    }
  }
};
