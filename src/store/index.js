import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      username : null,
      systemDate : null,
      userId : null,
      rolId : null,
      userKey : null,
      office: null,
      officeId: null,
      supervisorId : null,
      supervisorName : null
     
    },
    mutations: {
      setUsername (state, username) {
        state.username = username
      },
      setSystemDate(state , date){
        state.systemDate = date
      },
      setUserId(state, userId){
        state.userId = userId
      },
      setUserKey(state, userKey){
        state.userKey = userKey
      },
      setOffice(state, office){
        state.office = office
      },
      setOfficeId(state, officeId){
        state.officeId = officeId
      },
      setRolId(state, rol){
        state.rolId  = rol 
      },
      setSupervisorId(state, supervisorId){
        state.supervisorId = supervisorId
      },
      setSupervisorName(state, supervisorName){
        state.supervisorName = supervisorName
      }

    },


    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  return Store
}
